FROM php:8.2-fpm-alpine3.18

ENV COMPOSER_ALLOW_SUPERUSER 1
ENV TZ ${TZ:-"Europe/Moscow"}
ENV PATH /opt/ffmpeg/bin:$PATH
ENV PHP_OPCACHE_VALIDATE_TIMESTAMPS ${PHP_OPCACHE_VALIDATE_TIMESTAMPS:-0}
ENV PHP_OPCACHE_MAX_ACCELERATED_FILES ${PHP_OPCACHE_MAX_ACCELERATED_FILES:-10000}
ENV PHP_OPCACHE_MEMORY_CONSUMPTION ${PHP_OPCACHE_MEMORY_CONSUMPTION:-192}
ENV PHP_OPCACHE_MAX_WASTED_PERCENTAGE ${PHP_OPCACHE_MAX_WASTED_PERCENTAGE:-10}

RUN apk add --update --no-cache tzdata git mc zip wget curl shadow bash

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions

RUN install-php-extensions imagick gd exif intl zip calendar memcached pcntl sockets xsl uuid gettext opcache bcmath mysqli pdo_mysql 

COPY symfony-cli_5.4.8_x86_64.apk /srv/symfony-cli_5.4.8_x86_64.apk

RUN set -xe \
    && apk add --no-cache --update --virtual .phpize-deps $PHPIZE_DEPS \
    && apk add --allow-untrusted /srv/symfony-cli_5.4.8_x86_64.apk \
    && rm -rf /srv/symfony-cli_5.4.8_x86_64.apk \
    && rm -rf /usr/share/php \
    && rm -rf /tmp/* \
    && apk del  .phpize-deps


COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

COPY conf.d/php-extend.ini /usr/local/etc/php/conf.d/php-extend.ini
COPY conf.d/opcache.ini /usr/local/etc/php/conf.d/opcache.ini
COPY conf.d/errors.ini /usr/local/etc/php/conf.d/errors.ini

COPY --from=registry.gitlab.com/yoggica/docker-images/ffmpeg:latest /usr/local /usr/local

EXPOSE 9000

WORKDIR "/srv/app"

